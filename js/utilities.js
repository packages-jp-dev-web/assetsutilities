//screen
function printMessageSuccess(idfield, message, colormessage) {
  var success = "#ffffff";
  switch (colormessage) {
    case 'success':
      $(idfield).css("color", '#28a745');
      break;
    case 'error':
      $(idfield).css("color", '#dc3545');
      break;
    case 'primary':
      $(idfield).css("color", '#007bff');
      break;
    default:
      $(idfield).css("color", colormessage);
  }
  $(idfield).html(message);
}
function disabledButton(selector, classAditional) {
  $(selector).attr("disabled", "disabled");
  $(selector).addClass(classAditional);
}
function enabledButton(selector, classAditional) {
  $(selector).removeAttr("disabled");
  $(selector).removeClass(classAditional);
}
// Requisitions
function makeHeader() {
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      //contentType:'application/x-www-form-urlencoded; charset=UTF-8'
      contentType: "application/json; charset=UTF-8"
    }
  });
}
function requisition(url, type, dataType, parameters, funcAfter) {
  makeHeader();
  $.ajax({
    url: url,
    data: parameters,
    type: type,
    dataType: dataType,
    success: function (data) {
      window[funcAfter](data);
    },
    error: function (request, status, erro) {
      //window[funcAfter]({result: 'error', message: erro + 'status = ' + status}); // 'Erro na solicitação. Tente novamente mais tarde!'});
      window[funcAfter]({
        result: "error",
        message: "Desculpe, algo deu errado. Tente novamente mais tarde."
      });
    }
  });
}
//end Requistions
// Treatement Errors Laravel
/**
 * Seta as mensagens de erro de validação
 * @param {array} data
 * @returns {void}
 */
function treatementErrorsLaravel(data) {
  var sufix = data.sufix;
  var errors = data.errors;
  for (var key in errors) {
    if (data.sufix !== undefined) {
      $("#error-" + key + data.sufix).html(" ");
      $("#error-" + key + data.sufix).html(errors[key]);
    } else {
      $("#error-" + key).html(" ");
      $("#error-" + key).html(errors[key][0]);
    }
  }
}
//end Treatement Errors Laravel
/**
 * faz a requisição de busca de cep automático
 * @param {string} cep
 * @param {string} afterFunction
 * @returns {mixed}
 */
function serchAddressForCep(cep, afterFunction) {
  var result = [];
  var cepFormated = cep.replace(/\D/g, "");
  if (cepFormated === "") {
    result["result"] = "empty";
    window[afterFunction](result);
    return;
  }
  var validacep = /^[0-9]{8}$/;
  if (!validacep.test(cepFormated)) {
    result["result"] = "invalid";
    window[afterFunction](result);
    return;
  }
  $.getJSON(
    "https://viacep.com.br/ws/" + cepFormated + "/json/?callback=?",
    function (dados) {
      result["result"] = "complet";
      result["data"] = dados;
      window[afterFunction](result);
      return;
      /*Format dados
         "cep": "01001-000",
         "logradouro": "Praça da Sé",
         "complemento": "lado ímpar",
         "bairro": "Sé",
         "localidade": "São Paulo",
         "uf": "SP",
         "unidade": "",
         "ibge": "3550308",
         "gia": "1004"   
         
         With error
         "erro": true
         */
    }
  );
}
