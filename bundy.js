var bundy = require('bundy');

bundy.js([
    'js/utilities.js',
    'node_modules/cleave.js/dist/cleave.min.js',
    'node_modules/cleave.js/dist/addons/cleave-phone.br.js',
    'js/masked.js',
    'js/screen.js'
], 'js/assetsutilities.min.js');

bundy.build();